import random

options=['r','s','p']

userPoints=0

computerPoints=0

while userPoints<3 and computerPoints<3:
    computerChoice=random.choice(options)

    userChoice=input("Enter your choice: (r,p,s)\n")

    print("You chose "+userChoice+". The computer chose "+computerChoice)

    if userChoice==computerChoice:
        print("It's a draw!")
        pass
    elif (userChoice=='p' and computerChoice=='r') or (userChoice=='s' and computerChoice=='p') or (userChoice=='r' and computerChoice=='s'):
        print("You win this round!")
        userPoints+=1
    else:
        print("The computer wins this round!")
        computerPoints+=1
if userPoints==3:
    print("You win the game!")
else:
    print("The computer wins the game!")